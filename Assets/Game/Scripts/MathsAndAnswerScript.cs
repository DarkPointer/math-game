﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script manages the question , ans and to which button the ans is ti be assigned
/// </summary>

public class MathsAndAnswerScript : MonoBehaviour {

    //we make this script instance
    public static MathsAndAnswerScript instance;

    //its an enum which we help use to identify the current mode of game 
    public enum MathsType
    {
       Easy,
       Medium,
       Hard
    }



    public enum Level
    {
        Easy = 0,
        Medium,
        Hard
    }


    //we make a variable of MathsType
    public MathsType mathsType;

    //2 private floats this are the question values a and b
    private float a, b ;
    //the variable for answer value
    [HideInInspector] public float answer;
    //varible whihc will assign ans to any one of the 4 answer button
    private float locationOfAnswer;
    //ref to the button
    public GameObject[] ansButtons;
    //ref to image symbol so player can know which operation is to be done
    public Image mathSymbolObject;
    //ref to all the symbol sprites whihc will be used in above image
    public Sprite[] mathSymbols;
    //get the tag of button 
    public string tagOfButton;

    //varible to check whihc mode is this
    private int currentMode;

    //ref to the time for each question
    public float timeForQuestion;

    //score vairable
    [HideInInspector]public int score;

    //ref to text in scene where we will assign a and b values of question
    public Text valueA , valueB, valueC;

    //this is to check the progress of player so we can decrease the time with increase in score
    private float scoreMileStone;
    public float scoreMileStoneCount;


    void Awake()
    {
        MakeInstance();
    }

    //method whihc make this object instance
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    //at start we need to do few basic setups
	void Start ()
    {
        //we put the location value in tag of button variable
        tagOfButton = locationOfAnswer.ToString();

        //at start the mileSton value is equal to mile stone count
        scoreMileStone = scoreMileStoneCount;

        //get the time value
        GameManager.singleton.timeForQuestion = timeForQuestion;

        if (GameManager.singleton != null)
        {
            //get whihc mode is selected
            currentMode = GameManager.singleton.currentMode;
        }

        //we call the methods
        CurrentMode();

        MathsProblem();

    }

    //this method keeps the track of mode 
    void CurrentMode()
    {
        if (currentMode == 1)
        {
            //depending on the currentmode value we assign the mode
            mathsType = MathsType.Easy;

        }
        else if (currentMode == 2)
        {
            mathsType = MathsType.Medium;
        }
        else if (currentMode == 3)
        {
            mathsType = MathsType.Hard;
        }
      
    }
	
	// Update is called once per frame
	void Update ()
    {
        tagOfButton = locationOfAnswer.ToString();

      //  MileStoneProcess();  //no need for decrease
    }

    //this method reduces the time with increase in score
    void MileStoneProcess()
    {
        if (scoreMileStone < GameManager.singleton.currentScore)
        {
            scoreMileStone += scoreMileStoneCount;

            timeForQuestion += 0.02f;

            if (timeForQuestion >= 0.2f)
            {
                timeForQuestion = 0.2f;
            }

        }
    }


    //Below code is for maths calculation
    //this methode calls the respective method for the respective mode
    //eg for addition mode it will only call addition method
    public void MathsProblem()
    {
        //switch case is used to assign method
        switch (mathsType)
        {
            case (MathsType.Easy):
                GameManager.singleton.timeForQuestion = 0.08f;


                Easy();

                break;

            case (MathsType.Medium):
                GameManager.singleton.timeForQuestion = 0.06f;
                Medium();

                break;

            case (MathsType.Hard):
                GameManager.singleton.timeForQuestion = 0.04f;
                Hard();

                break;


          



              

        }
    }



    public string GenerateFunction(Level lvl, ref double result,ref string equal)
    {
        double answer = 0;
        int first = 0;
        int second = 0;
        int secondside = 0;

        if (lvl == Level.Easy)
        {
            first = 1; // Always x , no 2x , 3x etc.
            second = Random.Range(1, 10);
            secondside = second + Random.Range(1, 5);
            answer = (double)(secondside - second) / (double)first;

        }
        else if (lvl == Level.Medium)
        {
            first = Random.Range(1, 5);
            second = Random.Range(1, 100);
            secondside = second + Random.Range(-10, -10);
            answer = (double)(secondside - second) / (double)first;

        }
        else if (lvl == Level.Hard)
        {
            first = Random.Range(-10, 10);
            second = Random.Range(-100, 100);
            secondside = second + Random.Range(-50, 50);
            answer = (double)(secondside - second) / (double)first;

        }

        result = System.Math.Round(answer, 1);

        string function = first + "x " + second ;
        if (second >= 0)
        {

            function = first + "x + " + second ;
        }

        equal = secondside.ToString();

        return function;
    }

    // Addition
    //this methode perform addition process
    void Easy()
    {
        //we assign the random number to a and b , it range from 0 - 21
      //  a = Random.Range(0, 21);
     //   b = Random.Range(0, 21);

        //we the assign the location of answer a random number from our total number of buttons
        locationOfAnswer = Random.Range(0, ansButtons.Length);
        double fanswer = 0;
        string equal="";
        string func = GenerateFunction(Level.Easy, ref fanswer, ref equal);

        //we get the answer value
        answer = (float)fanswer;

        //the question values are assigned to question text
		valueA.text = "" + func.Replace("1x","x"); //temporary solution to change 1x + 7 = bla to x + 7 = bla (removing 1 simply)
        valueC.text = "" + equal;

        //and we assign the math symbol to symbol image
    //    mathSymbolObject.sprite = mathSymbols[0];

        //now we assign the values to the ans buttons
        for (int i = 0; i < ansButtons.Length; i++)
        {
            if (i == locationOfAnswer)
            {
                //we check for location value and the assign it to the corresponding ans button 
                ansButtons[i].GetComponentInChildren<Text>().text = "" + answer;

            }
            else
            {
                //for other ans button we assign random values
                ansButtons[i].GetComponentInChildren<Text>().text = "" + Random.Range(1,10);

                while (ansButtons[i].GetComponentInChildren<Text>().text == "" + answer)
                {
                    //we make sure that only one button has answer values 
                    ansButtons[i].GetComponentInChildren<Text>().text = "" + Random.Range(1, 10);
                }
            }

        }
        
     }
    // Addition

    //Subtraction
    //this methode perform Subtraction process
    void Medium()
    {
        //we assign the random number to a and b , it range from 0 - 21
        //  a = Random.Range(0, 21);
        //   b = Random.Range(0, 21);

        //we the assign the location of answer a random number from our total number of buttons
        locationOfAnswer = Random.Range(0, ansButtons.Length);
        double fanswer = 0;
        string equal = "";
        string func = GenerateFunction(Level.Medium, ref fanswer, ref equal);

        //we get the answer value
        answer = (float)fanswer;

        //the question values are assigned to question text
        valueA.text = "" + func;
        valueC.text = "" + equal;

        //and we assign the math symbol to symbol image
        //    mathSymbolObject.sprite = mathSymbols[0];

        //now we assign the values to the ans buttons
        for (int i = 0; i < ansButtons.Length; i++)
        {
            if (i == locationOfAnswer)
            {
                //we check for location value and the assign it to the corresponding ans button 
                ansButtons[i].GetComponentInChildren<Text>().text = "" + answer;

            }
            else
            {
                //for other ans button we assign random values
                ansButtons[i].GetComponentInChildren<Text>().text = "" + Random.Range(-10, 10);

                while (ansButtons[i].GetComponentInChildren<Text>().text == "" + answer)
                {
                    //we make sure that only one button has answer values 
                    ansButtons[i].GetComponentInChildren<Text>().text = "" + Random.Range(-10, 10);
                }
            }

        }

    }
    //Subtraction


    //Multiplication
    //this methode perform Multiplication process
    void Hard()
    {
        //we assign the random number to a and b , it range from 0 - 21
        //  a = Random.Range(0, 21);
        //   b = Random.Range(0, 21);

        //we the assign the location of answer a random number from our total number of buttons
        locationOfAnswer = Random.Range(0, ansButtons.Length);
        double fanswer = 0;
        string equal = "";
        string func = GenerateFunction(Level.Hard, ref fanswer, ref equal);

        //we get the answer value
        answer = (float)fanswer;

        //the question values are assigned to question text
        valueA.text = "" + func;
        valueC.text = "" + equal;

        //and we assign the math symbol to symbol image
        //    mathSymbolObject.sprite = mathSymbols[0];

        //now we assign the values to the ans buttons
        for (int i = 0; i < ansButtons.Length; i++)
        {
            if (i == locationOfAnswer)
            {
                //we check for location value and the assign it to the corresponding ans button 
                ansButtons[i].GetComponentInChildren<Text>().text = "" + answer;

            }
            else
            {
                //for other ans button we assign random values
                ansButtons[i].GetComponentInChildren<Text>().text = "" + Random.Range(-100, 100);

                while (ansButtons[i].GetComponentInChildren<Text>().text == "" + answer)
                {
                    //we make sure that only one button has answer values 
                    ansButtons[i].GetComponentInChildren<Text>().text = "" + Random.Range(-100, 100);
                }
            }

        }

    }
  



}
